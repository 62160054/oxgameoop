/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.xogameoop;

import java.util.Scanner;

/**
 *
 * @author Memos
 */
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;
    Scanner kb = new Scanner(System.in);
    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public void showTable(){
        table.showTable();
    }
    public void input(){
        while(true){
            System.out.println("Please input row col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if(table.setRowCol(row, col)){
                break;
            }
            System.out.println("Error : Table at row and col is not empty !!!");
        }
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName() + " Turn");
    }
    public void run(){
        this.showWelcome();
        while(true){
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFinish()){
                if(table.getWinner() == null){
                    System.out.println("Draw !!");
                }else{
                    System.out.println(table.getWinner().getName() + " Win !!");
                }
                break;
            }
            table.switchPlayer();
        }
    }
}
